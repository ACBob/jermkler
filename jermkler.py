import sys
import os
import toml

import datetime
import random

from discord.ext import tasks
import discord

# Strings we use to index the config
# I'm sure datetime have their own but this is just easier for a rushed joke bot :)
DAYS = [
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday",
    "sunday"
]


def load_config() -> dict:
    if not os.path.exists('config.toml'):
        print("Please create config.toml in the working directory")
        sys.exit(1)

    with open('config.toml', 'r') as f:
        return toml.load(f)


class JermklerClient(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        with open("token.txt", "r") as f:
            self.token = f.read().strip()

        self.config = load_config()

        self.weekday = datetime.datetime.now().today().weekday()

    async def setup_hook(self) -> None:
        print("startin'")
        if not self.do_update_cycle.is_running():
            self.do_update_cycle.start()

    async def on_ready(self):
        print("Logged in as")
        print(self.user.name)
        print(self.user.id)
        print("------")

        await self.setup_hook()

    @tasks.loop(hours=1)
    async def do_update_cycle(self):
        # We can assume every time the weekday changes, it's a new day.
        weekday = datetime.datetime.now().today().weekday()
        if weekday != self.weekday:
            self.weekday = weekday
            await self.sparkle()

    # NOTE: it's actually really dumb to let the bot do this if it's intended for multiple servers
    # async def on_message(self, message):
    #     if message.content.startswith(self.config["misc"]["prefix"] + "force"):
    #         if message.author.guild_permissions.administrator:
    #             await message.channel.send("Ok!")
    #             await self.sparkle()


    @do_update_cycle.before_loop
    async def before_update(self):
        print("waitin'")
        await self.wait_until_ready()  # wait until the bot is ready

    async def sparkle(self):
        # Figure out day to send
        image = random.choice(self.config["days"][DAYS[self.weekday]])

        # now we open it...
        imagef = discord.File(image)

        for channel_id in self.config["misc"]["channels"]:
            chan = self.get_channel(channel_id)
            await chan.send(file = imagef)

if __name__ == '__main__':
    if not os.path.exists('token.txt'):
        print("Please specify bot token in token.txt in the working directory. It should contain the discord bot token.")
        sys.exit(1)

    client = JermklerClient(intents=discord.Intents.default())
    client.run(client.token)